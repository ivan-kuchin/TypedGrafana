import { BooleanParameter, Context, GrafanaObj, StringMap, StringOptionMap, StringParameter } from '../base_types'
import { assert } from 'console'
import { Datasource } from './datasource'

export enum VariableRefresh {
    Never = 0,
    OnDashboardLoad = 1,
    OnTimeRangeChange = 2,
}

export enum VariableSort {
    Disabled = 0,
    AlphabeticalAsc = 1,
    AlphabeticalDesc = 2,
    NumericalAsc = 3,
    NumericalDesc = 4,
    AlphabeticalCaseInsensitiveAsc = 5,
    AlphabeticalCaseInsensitiveDesc = 6,
}

interface CustomVariableOptions {
    name: StringParameter
    multi?: BooleanParameter
    includeAll?: BooleanParameter
    allValue?: StringParameter
}

export interface QueryVariableOptions {
    name: StringParameter
    label?: StringParameter
    datasource?: Datasource
    query?: StringParameter
    regex?: StringParameter
    refresh?: VariableRefresh
    sort?: VariableSort
    multi?: BooleanParameter
    includeAll?: BooleanParameter
    allValue?: StringParameter
}

// Marker interface
export abstract class Variable extends GrafanaObj {
}

// In Grafana, these are called "Options". Unfortunately, we've already used
// "options" as the main container in GrafanaObj, so we can't re-use it here.
// Thus I've opted to go with "choices" instead. Apologies for the confusion.
type Choice = { text: StringParameter, value: StringParameter }

export class CustomVariable extends Variable {
    options: StringOptionMap & CustomVariableOptions
    static defaults: CustomVariableOptions = {
        name: '',
        multi: false,
        includeAll: false,
        allValue: null,
    }

    choices: Choice[]

    constructor(opts: CustomVariableOptions) {
        super()
        this.choices = []
        this.options = { ...CustomVariable.defaults, ...opts }
        this.options['type'] = 'custom'
        this.options['query'] = 'ams1,ams2'
    }

    addOption(opts: { value: StringParameter, text?: StringParameter }) {
        this.choices.push({ value: opts.value, text: opts.text || opts.value })
        return this
    }

    postRender(current: object, c: Context): StringMap<any> {
        if (this.options.includeAll) {
            this.choices.unshift({ text: 'All', value: '$__all' })
        }

        assert(this.choices.length > 0, `When creating a custom variable you must provide at least one option (variable name: ${this.options.name})`)

        let choices = this.choices.map((choice) => {
            return { text: c.resolve(choice.text), value: c.resolve(choice.value), selected: true }
        })

        return { ...current, options: choices, current: this.generateCurrent(choices) }
    }

    generateCurrent(choices: Choice[]): { text: String, value: String[] } {
        if (this.options.multi) {
            let values = choices.map((choice) => choice.value as String)
            return { text: values.join(' + '), value: values }
        } else {
            let choice = choices[0]
            return { text: choice.text as String || choice.value as String, value: [choice.value as String] }
        }
    }
}

export class QueryVariable extends Variable {
    options: StringOptionMap & QueryVariableOptions
    static defaults: QueryVariableOptions = {
        name: '',
    }

    constructor(opts: QueryVariableOptions) {
        super()
        this.options = { ...QueryVariable.defaults, ...opts }
        this.options['type'] = 'query'
    }

    postRender(current: object, c: Context): StringMap<any> {
        return { ...current, current: { text: 'All', value: '$__all' } }
    }
}
